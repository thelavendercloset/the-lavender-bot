# The Lavender Bot
A Discord Bot for the TLC Discord Server/Guild

### config.py
```python
token = '<TOKEN>'
prefix = '<COMMAND PREFIX>'

roleMsg = [
    {"msgID": <MESSAGE ID>, "reaction": "<EMOJI>", "roleID": <ROLE ID>}
]
bannedRoleID = <BANNED ROLE ID>
```