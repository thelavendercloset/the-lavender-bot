try:
    import config

    if not config.token or not config.prefix:
        raise ImportError
except ImportError:
    configFile = open('config.py', 'w+')
    configFile.write("token = ''\nprefix = '!'\n\nroleMsg = []\nbannedRoleID = None")
    exit()

import discord

intents = discord.Intents().default()
intents.members = True

bot = discord.Client(intents=intents)


@bot.event
async def on_ready():
    print("Logged on as {0.user}".format(bot))


@bot.event
async def on_raw_reaction_add(payload):
    banned_role = discord.utils.get(bot.get_guild(payload.guild_id).roles, id=config.bannedRoleID)
    if banned_role in payload.member.roles:
        return
    for e in config.roleMsg:
        if payload.message_id == e['msgID'] and payload.emoji.name == e['reaction']:
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles, id=e['roleID'])
            await payload.member.add_roles(role)


@bot.event
async def on_raw_reaction_remove(payload):
    for e in config.roleMsg:
        if payload.message_id == e['msgID'] and payload.emoji.name == e['reaction']:
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles, id=e['roleID'])
            await bot.get_guild(payload.guild_id).get_member(payload.user_id).remove_roles(role)


bot.run(config.token)
